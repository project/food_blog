ABOUT FOOD BLOG
-----------

Food Blog is a custom theme that provides many classes in its markup. For cleaner
markup (fewer classes), the default Stable custom theme can be used instead.

To use Food Blog as your custom theme, set the 'custom theme' in your theme's .info.yml
file to "Food Blog":
  custom theme: Food Blog

See https://www.drupal.org/docs/8/theming-drupal-8/using-Food Blog-as-a-custom-theme
for more information on using the Food Blog theme.



ABOUT BOOKING SECTION
----------------------

In this section we have Order Now button and also contact info.
Add view class name items


ABOUT FOOD IMAGES SECTION
----------------------

We have created seperate view for all sections on home page.
Add view class name services


ABOUT BOOK TABLE SECTION
--------------------

Add view class name book-table



ABOUT BLOGS SECTION
--------------------

In this we have created blogs, which can be auto updated according to their dates.
Blogs section has slider.

ABOUT MENUS SECTION
--------------------

In this we have divided number of items according to their category.
This section has tab function.





ABOUT DRUPAL THEMING
--------------------

See https://www.drupal.org/docs/8/theming for more information on Drupal
theming.
