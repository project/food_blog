$ = jQuery;
$(document).ready(function() {
  //nav
  var hasSubMenu = jQuery('.nav-menu').children('li.nav-item.nav-item--expanded').children('ul').hasClass('nav-menu-sub')
  if(hasSubMenu) {
    jQuery('.nav-menu > li.nav-item.nav-item--expanded').prepend('<span class="arrow-icon"></span>')
  }
  $('.arrow-icon').on('click', function() {
    $(this).parent('li.nav-item.nav-item--expanded').toggleClass("collapsed"); 
   
  });

  //menus
  //$('.all-content').show();
   $('.accord-outer').show();
  $('#navbar li a').click(function () {    
      var t = $(this).attr('id');
      console.log(t);
    if ($(this).hasClass('inactive')) { //this is the start of our condition 
          $('#navbar li a').addClass('inactive');
          $(this).removeClass('inactive');
          $('.accord-outer').hide();
        $('.' + t + '-content').fadeIn('slow');
          // if(t == 'ott-tab') {
          //     $(".ott-wrapper .accord-content:first").show();
          // }
      }
  });

      // //tabmenu
      // var i, tabcontent, tablinks,v;
      // tabcontent = document.querySelectorAll(".tabcontent-main");
      // tabcontent[0].style.display = "none";
      // tablinks = document.querySelectorAll("tablinks-main li");
      //  tablinks.forEach(function(tab,i){
      //      tab.addEventListener('click',function(){
      //          for(v=0 ; v< tablinks.length; v++){
      //              tabcontent[v].style.display = "none";
      //          }
               
      //          tabcontent[i].style.display = 'block';
      //      })
      //  })
  


      //slick-weekly
      $('.weekly-list > ul ').slick({
        slidesToShow: 3,
        speed: 300,
        infinite: false,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
          ],
      });

          //blogs
      $('.blogs-lists').slick({
        slidesToShow: 3,
        speed: 800,
        infinite: false,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              arrows: false,
              dots: true,
            },
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              dots: true,
            },
          },
        ],
      });


       
      




        


        $(function() {
          $(".arrow-icon").click(function() {
              $(".nav-menu-sub").slideToggle(300);
          });
    }); 



        // //active class nav
        // $('.nav-item').each(function () {
        //   if (this.href === path) {
        //     $(this).addClass('active');
        //   }
        // });




        
    });

//hamburger
var hamburger = document.querySelector(".hamburger");
var navMenu = document.querySelector(".nav-menu");

hamburger.addEventListener("click", mobileMenu);

function mobileMenu() {
    hamburger.classList.toggle("active");
    navMenu.classList.toggle("active");
}
var navLink = document.querySelectorAll(".nav-link");

navLink.forEach(n => n.addEventListener("click", closeMenu));

function closeMenu() {
    hamburger.classList.remove("active");
    navMenu.classList.remove("active");
}



